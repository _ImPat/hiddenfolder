import os
import hashlib
import subprocess
from tkinter import *
from tkinter import messagebox
import shutil

cwd = os.getcwd()


def hash_function(to_hash):
    return hashlib.sha256(to_hash.encode("utf-8")).hexdigest()


def get_folder_identifier(key, second):
    return hash_function(hash_function(key) + hash_function(second))[:7]


def invalid_entry():
    messagebox.showerror("Invalid Entry", "Your Folder key/name is not acceptable due to spaces or special characters.")
    return True


def open_dir(full_dir):
    if os.path.isdir(full_dir):
        subprocess.Popen("explorer " + full_dir)
    else:
        os.mkdir(full_dir)
        os.system("start /wait cmd /c attrib +s +h \"" + full_dir + "\"")
        subprocess.Popen("explorer " + full_dir)


def del_dir(del_path):
    if os.path.isdir(del_path):
        shutil.rmtree(del_path)
        messagebox.showinfo("Success!", "Folder deleted successfully.")
    else:
        messagebox.showerror("Unknown Folder", "Failed to find folder name with that key/name.")


def open_folder(folder_name, folder_key):
    if not re.match("^[A-Za-z0-9]*$", folder_name):
        invalid_entry()
        return 1
    elif folder_key == "":
        invalid_entry()
        return 1
    if folder_name == "":
        full_folder_name = cwd + "\\hidden_" + get_folder_identifier(folder_key, "hidden_folder")
        open_dir(full_folder_name)
    else:
        full_folder_name = cwd + "\\hidden_" + folder_name + "_" + get_folder_identifier(folder_key, folder_name)
        open_dir(full_folder_name)


def delete_folder(folder_name, folder_key):
    if not re.match("^[A-Za-z0-9]*$", folder_name):
        invalid_entry()
        return 1
    elif folder_key == "":
        invalid_entry()
        return 1
    if folder_name == "":
        full_folder_name = cwd + "\\hidden_" + get_folder_identifier(folder_key, "hidden_folder")
        del_dir(full_folder_name)
    else:
        full_folder_name = cwd + "\\hidden_" + folder_name + "_" + get_folder_identifier(folder_key, folder_name)
        del_dir(full_folder_name)


master = Tk()
master.title("Hidefolder")
master.resizable(0,0)
Label(master, text="Folder Name (Optional)").grid(row=0)
Label(master, text="Folder Key").grid(row=1)

e1 = Entry(master)
e2 = Entry(master)

e2.config(show="")

e1.grid(row=0, column=1)
e2.grid(row=1, column=1)


Button(master, text='Delete', command=lambda: delete_folder(e1.get(), e2.get())).grid(row=3, column=0, sticky=W, pady=4)
Button(master, text="Create/Open", command=lambda: open_folder(e1.get(), e2.get())).grid(row=3, column=1, sticky=W,
                                                                                         pady=4)

mainloop()
